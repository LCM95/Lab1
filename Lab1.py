#Escriba un programa de alquiler de vehículos que le permita al usuario seleccionar una categoría y un modelo de vehículo
# por una cantidad de días e imprima el monto a pagar a la agencia, el sistema debe identificar si el cliente es frecuente o no
# para aplicarle un descuento y debe cobrar una póliza de seguro, además si el cliente requiere el vehículo más tiempo del establecido
# el costo por día tendrá un incremento dependiendo del tipo de vehículo, tenga en cuenta los siguientes parámetros: (Python) 45 puntos
#Clase Modelo Días máximo precio Normal
#Precio por día Precio por día extra
#Póliza Descuento Cliente Frecuente Automóvil >= 2015 7 13,000.00 15%  10% 3% Automóvil >=2001 < 2014 5 18,000.00 10%
# 10% 3% Automóvil >=1990 <2000 7 10,000.00 5%  30% 10% Hatchback >= 2015 8 20,000.00 10%  8% 5% Hatchback >=2000 < 2014 6
#  17,000.00 5%     gugyug  9% 3% Doble Tracción > 2015 7 20,000.00 15%  11% 10% Doble Tracción > 2000 <= 2015 4 16,000.00 5%  10% 5%


print("   Mi nombres es Lilliam,¡Bienvenidos ")
nombre=input("Con quién tengo el gusto, me puede decir su nombre por favor: \n")

print("hola",nombre,"\n","Tenemos varios autos que puede alquilar, le voy a decir algunas categorías y modelos que hay disponibles:")

def opcion1():
    dias = int(input("Por cuantos días lo quiere alquilar: "))

    if dias <= 7:
        precio = dias * 13000
        poliza = 13000 * 0.10
        descuento = precio * 0.03
        pago = precio - descuento + poliza
        print("Por el alquiler tiene una Poliza de Seguro que pago", poliza, "y total es de", pago, "por", dias)
    else:
        diaNormal = 7 * 13000
        restarDia = dias - 7
        diasExtras = restarDia * 13000
        precio = diasExtras * 0.15
        total = diaNormal + precio
        poliza = 13000 * 0.10
        descuento = total * 0.03
        pago = total - descuento + poliza
        print("Por el alquiler tiene una Poliza de Seguro que pago", poliza, "y total es de", pago, "por", dias)


def opcion2():
    dias = int(input("Por cuantos días lo quiere alquilar: "))

    while dias <=5:
        precio = dias * 18000
        poliza = 13000 * 0.10
        descuento = precio * 0.03
        pago = precio - descuento + poliza
        print("Por el alquiler tiene una Poliza de Seguro que pago", poliza, "y total es de", pago, "por", dias)
        return
    if dias>=6:
        diaNormal = 5 * 18000
        restarDia = dias - 5
        diasExtras = restarDia * 18000
        precio = diasExtras * 0.10
        total = diaNormal + precio
        poliza = 13000 * 0.10
        descuento = total * 0.03
        pago = total - descuento + poliza
        print("Por el alquiler tiene una Poliza de Seguro que pago", poliza, "y total es de", pago, "por", dias)
        return

def opcion3():
    dias = int(input("Por cuantos días lo quiere alquilar: "))

    while dias <= 7:
        precio = dias * 10000
        poliza = 13000 * 0.30
        descuento = precio * 0.10
        pago = precio - descuento + poliza
        print("Por el alquiler tiene una Poliza de Seguro que pago", poliza, "y total es de", pago, "por", dias)
        return
    if dias >= 8:
        diaNormal = 5 * 10000
        restarDia = dias - 7
        diasExtras = restarDia * 10000
        precio = diasExtras * 0.05
        total = diaNormal + precio
        poliza = 13000 * 0.30
        descuento = total * 0.10
        pago = total - descuento + poliza
        print("Por el alquiler tiene una Poliza de Seguro que pago", poliza, "y total es de", pago, "por", dias)
        return
def opcion4():
    dias = int(input("Por cuantos días lo quiere alquilar: "))

    while dias <= 8:
        precio = dias * 20000
        poliza = 20000 * 0.08
        descuento = precio * 0.05
        pago = precio - descuento + poliza
        print("Por el alquiler tiene una Poliza de Seguro que pago", poliza, "y total es de", pago, "por", dias)
        return
    if dias >= 7:
        diaNormal = 5 * 20000
        restarDia = dias - 8
        diasExtras = restarDia * 20000
        precio = diasExtras * 0.10
        total = diaNormal + precio
        poliza = 20000 * 0.08
        descuento = total * 0.05
        pago = total - descuento + poliza
        print("Por el alquiler tiene una Poliza de Seguro que pago", poliza, "y total es de", pago, "por", dias)
        return
def opcion5():
    dias = int(input("Por cuantos días lo quiere alquilar: "))

    while dias <= 6:
        precio = dias * 17000
        poliza = 17000 * 0.09
        descuento = precio * 0.3
        pago = precio - descuento + poliza
        print("Por el alquiler tiene una Poliza de Seguro que pago", poliza, "y total es de", pago, "por", dias)
        return
    if dias >= 7:
        diaNormal = 5 * 17000
        restarDia = dias - 6
        diasExtras = restarDia * 17000
        precio = diasExtras * 0.05
        total = diaNormal + precio
        poliza = 13000 * 0.09
        descuento = total * 0.3
        pago = total - descuento + poliza
        print("Por el alquiler tiene una Poliza de Seguro que pago", poliza, "y total es de", pago, "por", dias)
        return
def opcion6():
    dias = int(input("Por cuantos días lo quiere alquilar: "))

    while dias <= 7:
        precio = dias * 20000
        poliza = 20000 * 0.10
        descuento = precio * 0.3
        pago = precio - descuento + poliza
        print("Por el alquiler tiene una Poliza de Seguro que pago", poliza, "y total es de", pago, "por", dias)
        return
    if dias >= 8:
        diaNormal = 5 * 20000
        restarDia = dias - 7
        diasExtras = restarDia * 20000
        precio = diasExtras * 0.15
        total = diaNormal + precio
        poliza = 13000 * 0.11
        descuento = total * 0.10
        pago = total - descuento + poliza
        print("Por el alquiler tiene una Poliza de Seguro que pago", poliza, "y total es de", pago, "por", dias)
        return
def opcion7():
    dias = int(input("Por cuantos días lo quiere alquilar: "))

    while dias <= 4:
        precio = dias * 16000
        poliza = 16000 * 0.10
        descuento = precio * 0.05
        pago = precio - descuento + poliza
        print("Por el alquiler tiene una Poliza de Seguro que pago", poliza, "y total es de", pago, "por", dias)
        return
    if dias >= 5:
        diaNormal = 4 * 16000
        restarDia = dias - 4
        diasExtras = restarDia * 16000
        precio = diasExtras * 0.05
        total = diaNormal + precio
        poliza = 13000 * 0.10
        descuento = total * 0.05
        pago = total - descuento + poliza
        print("Por el alquiler tiene una Poliza de Seguro que pago", poliza, "y total es de", pago, "por", dias)
        return


def menu():
    print("TENEMOS:\n",
          "1)AUTOMÓVIL, MODELO (2015).\n",
          "2)AUTOMÓVIL, MODELO (2001 a 2014).\n",
          "3)AUTOMÓVIL, MODELO (1990 a 200).\n",
          "4)HATCHBACK, MODELO (2015).\n",
          "5)HATCHBACK, MODELO (2000 a 2014).\n",
          "6)DOBLE TRACIÓN, MODELO (2015).\n",
          "7)DOBLE TRACIÓN, MODELO (2000 a 2015).\n",
          "8)MODELO (MAYOa A  2015 NO ESTAN REGISTRADO).\n",
          "9)SALIR")

    opcion = input("Seleccione la acción que quiere alquilar:")

    if opcion =="1":
        print("     Has eligido la opción, AUTOMÓVIL MODELO (2015)")
        print(" Precio normal dentro de los 7 días ( 13.000 colones) si es mayor se cobra (15%)")
        opcion1()

    elif opcion =="2":
        print("     AUTOMÓVIL, MODELO (2001 a 2014)")
        print(" Precio normal dentro de los 5 días ( 18.000 colones) si es mayor se cobra (10%)")
        opcion2()

    elif opcion =="3":
        print("     AUTOMÓVIL, MODELO (1990 a 200)")
        print(" Precio normal dentro de los 7 días ( 10.000 colones) si es mayor se cobra (5%)")
        opcion3()

    elif opcion =="4":
        print("     HATCHBACK, MODELO (2015)")
        print(" Precio normal dentro de los 8 días ( 20.000 colones) si es mayor se cobra (10%)")
        opcion4()

    elif opcion =="5":
        print("     HATCHBACK, MODELO (2000 a 2014)")
        print(" Precio normal dentro de los 6 días ( 17.000 colones) si es mayor se cobra (5%)")
        opcion5()

    elif opcion =="6":
        print("     DOBLE TRACIÓN, MODELO (2015)")
        print(" Precio normal dentro de los 7 días ( 20.000 colones) si es mayor se cobra (15%)")
        opcion6()

    elif opcion =="7":
        print("     DOBLE TRACIÓN, MODELO (2000 a 2015)")
        print(" Precio normal dentro de los 4 días ( 16.000 colones) si es mayor se cobra (5%)")
        opcion7()
    elif opcion == "9":
        print("No se encuentran registrado modelos de esas fecha")
        opcion8()

    elif opcion =="9":
        print("Gracias por la visita estimado",nombre,"por Starcars-Autos Usados en Costa Rica")
    else:
        print("Opción inválida.")


menu()




























